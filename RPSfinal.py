import random
rmd={}
print("\nLet's play a simple 10 round game of Rock Paper Scissors")
for i in range(0,10):
     print("\nRound {}".format(i+1))
     player_choice = input("Enter the choice (r for rock , s for scissors,p for paper) :")
     while(player_choice != 'r' and player_choice != 's' and player_choice != 'p' ):
         player_choice = input("Invalid entry, Try again(r,s,p):")
     computer_choice= random.choice(['r','p','s'])
     print("Computer choice is ",computer_choice)
     choice_dict = {'r':0,'p':1,'s':2}
     choice_ind=choice_dict.get(player_choice)
     comp_index = choice_dict.get(computer_choice)
     mat=[[0,2,1],
         [1,0,2],
         [2,1,0]]
     result_ind =mat[choice_ind][comp_index]
     res_mssg = ("It's a Tie","Player Won","Computer Won")
     result = res_mssg[result_ind]
     print(result)
     list=[player_choice, computer_choice, result]
     rmd[i+1]=list

print("\nCheck history\n")
con='yes'
while(con=='yes'):
    a=int(input("Which round do you want to check?"))
    print("Player choice:{}".format(rmd[a][0]))
    print("Computer choice:{}".format(rmd[a][1]))
    print("Winner of round:{}".format(rmd[a][2]))
    con = (input("Do you want to continue checking history?(yes/no):\n"))
