from random import randint
plays = ['rock','paper','scissors']
limit = 10
count = 1
his={}
print("\nLet's play a simple 10 round game of Rock Paper Scissors\n")

while count <= 10:
    print("Round {}".format(count))
    c = plays[randint(0, 2)]
    p=input("rock, paper or scissors: ")
    print("Computer chooses:{}".format(c))
    if p == c:
        print("tie\n")
        status = "Draw"
    else:
        if p == 'rock':
            if c == 'paper':
                print("Computer wins\n")
                status = "Computer"
            else:
                print("You win\n")
                status = "User"
        elif p == 'paper':
            if c == 'scissors':
                print("Computer wins\n")
                status = "Computer"
            else:
                print("You win\n")
                status = "User"
        elif p == 'scissors':
            if c == 'rock':
                print("Computer wins\n")
                status = "Computer"
            else:
                print("You win\n")
                status = "User"
        else:
            print("invalid entry\n")
            status = "Invalid round"

    round=[p,c,status]
    his[count]=round
    count=count+1

con='yes'
while con == 'yes':
    print("History of game\n")
    a=int(input("Enter round to get details:"))
    print("Player choice:{}\n".format(his[a][0]))
    print("Computer choice:{}\n".format(his[a][1]))
    print("Winner of round:{}\n".format(his[a][2]))
    con=(input("Do you want to continue checking history?(yes/no):"))

